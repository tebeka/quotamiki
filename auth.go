package main

import (
	"context"
	"encoding/json"
	"fmt"

	secretmanager "cloud.google.com/go/secretmanager/apiv1beta1"
	secretmanagerpb "google.golang.org/genproto/googleapis/cloud/secretmanager/v1beta1"
)

const (
	projectID = "quotamiki"
	secretID  = "twitter"
)

// loadTwitterCreds loads twitter credentials from Google's Secret Manager
// The credentials are one JSON encoded key
func loadTwitterCreds(ctx context.Context) (*TwitterCreds, error) {
	client, err := secretmanager.NewClient(ctx)
	if err != nil {
		return nil, err
	}

	keyName := fmt.Sprintf(
		"projects/%s/secrets/%s/versions/latest", projectID, secretID)

	accessRequest := &secretmanagerpb.AccessSecretVersionRequest{
		Name: keyName,
	}

	result, err := client.AccessSecretVersion(ctx, accessRequest)
	if err != nil {
		return nil, err
	}

	var tc TwitterCreds
	if err := json.Unmarshal(result.Payload.Data, &tc); err != nil {
		return nil, err
	}

	return &tc, nil
}

type TwitterCreds struct {
	AccessToken       string `json:"access_token"`
	AccessTokenSecret string `json:"access_token_secret"`
	AccessKey         string `json:"access_key"`
	APISecretKey      string `json:"api_secret_key"`
}
