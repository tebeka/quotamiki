# quotamiki - Twitter bot for quotes

[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)
[![CI](https://gitlab.com/tebeka/quotamiki/badges/master/pipeline.svg)](https://gitlab.com/tebeka/quotamiki/pipelines)


Publish a quote per day from text database to
[@quotamiki](https://twitter.com/quotamiki).

## Hacking
This is a [Go](https://golang.org/) application that runs on [Google App
Engine](https://cloud.google.com/appengine/docs/go).

### Quotes DB
The quotes are in `quotes.txt` and the script `gen_quotes.go` converts it to
`quotes.go` which has the quotes as a `[]string`.

Run `go generate` to update `quotes.go`.

### Authentication
Twitter credentials are stored in a JSON file which is loaded to Google Cloud's
[Secret Manager](https://cloud.google.com/secret-manager/docs). See
`update_auth.go`. Run `make update-auth` to update authentication.

### Cron
GAE cron will call the `cronHandler` in schedule defined in `cron.yaml`

## HTTP Server
The HTTP server exposes `/cron` to handler cron calls from GAE and `/quote`
that will return a random quote.
