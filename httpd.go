package main

//go:generate go run gen_quotes.go
//go:generate go fmt quotes.go

import (
	"context"
	"fmt"
	"log"
	"math/rand"
	"net/http"
	"os"
	"time"
)

var (
	rnd = rand.New(rand.NewSource(time.Now().Unix()))
)

func main() {
	http.Handle("/", http.FileServer(http.Dir("./static")))
	http.HandleFunc("/quote", quoteHandler)
	http.HandleFunc("/_/cron", cronHandler)

	port := os.Getenv("PORT")
	if port == "" {
		port = "8080"
	}
	addr := ":" + port

	log.Printf("INFO: listening on %s", addr)
	if err := http.ListenAndServe(addr, nil); err != nil {
		log.Fatal(err)
	}
}

func contains(quotes []string, quote string) bool {
	for _, q := range quotes {
		if q == quote {
			return true
		}
	}
	return false
}

func newRandQuote(ctx context.Context) (string, error) {
	quotes, err := postedQuotes(ctx)
	if err != nil {
		return "", err
	}

	quotes = newQuotes(quotes)
	if len(quotes) == 0 {
		// TODO: Delete & start over?
		return randElem(QuoteDB), nil
	}

	return randElem(quotes), nil
}

// called by GAE cron (see cron.yaml)
func cronHandler(w http.ResponseWriter, r *http.Request) {
	if !isInternalCall(r) {
		http.Error(w, "cron only", http.StatusUnauthorized)
		return
	}

	ctx := r.Context()
	creds, err := loadTwitterCreds(ctx)
	if err != nil {
		log.Printf("ERROR: can't load creds - %s", err)
		http.Error(w, "can't load creds", http.StatusInternalServerError)
		return
	}

	quote, err := newRandQuote(ctx)
	if err != nil {
		log.Printf("ERROR: can't get new quote - %s", err)
		http.Error(w, "can't get quote", http.StatusInternalServerError)
		return
	}

	id, err := postTweet(ctx, quote, creds)
	if err != nil {
		log.Printf("ERROR: can't post - %s", err)
		http.Error(w, "can't post", http.StatusInternalServerError)
		return
	}
	log.Printf("INFO: posted %s", id)

	if err := storeQuote(ctx, time.Now(), quote); err != nil {
		log.Printf("ERROR: can't store - %s", err)
	}

	fmt.Fprintf(w, "tweet ID: %s\n", id)
}

// GET /quote will return a random quote in plain text
func quoteHandler(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "%s\n", randElem(QuoteDB))
}

func slice2map(items []string) map[string]bool {
	m := make(map[string]bool)
	for _, s := range items {
		m[s] = true
	}
	return m
}

func newQuotes(existing []string) []string {
	m := slice2map(existing)
	var quotes []string
	for _, q := range QuoteDB {
		if m[q] {
			continue
		}
		quotes = append(quotes, q)
	}
	return quotes
}

func randElem(elems []string) string {
	n := rnd.Intn(len(elems))
	return elems[n]
}

func isInternalCall(r *http.Request) bool {
	return r.Header.Get("X-Appengine-Cron") != ""
}
