package main

import (
	"context"
	"testing"
	"time"

	"github.com/stretchr/testify/require"
)

func TestDB(t *testing.T) {
	// if os.Getenv("CI") != "" {
	if true { // FIXME: Need creds
		t.Skip("not working in CI")
	}
	require := require.New(t)
	quote := `My safe word is: keep going.
    - Pazuzu (Sisters)`

	ctx := context.Background()
	err := storeQuote(ctx, time.Now(), quote)
	require.NoError(err, "post")

	quotes, err := postedQuotes(ctx)
	t.Logf("%d quotes loaded", len(quotes))
	require.NoError(err, "postedQuotes")
	require.Contains(quotes, quote, "postQuote not found")
}
