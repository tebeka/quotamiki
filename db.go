package main

import (
	"context"
	"time"

	"cloud.google.com/go/datastore"
	"google.golang.org/api/iterator"
)

const (
	project    = "quotamiki"
	entityName = "Post"
)

type Post struct {
	Date  time.Time
	Quote string
}

func storeQuote(ctx context.Context, date time.Time, quote string) error {
	client, err := datastore.NewClient(ctx, project)
	if err != nil {
		return err
	}

	post := &Post{date, quote}

	parentKey := datastore.NameKey("Posts", "default", nil)
	key := datastore.IncompleteKey(entityName, parentKey)
	_, err = client.Put(ctx, key, post)
	return err
}

func postedQuotes(ctx context.Context) ([]string, error) {
	client, err := datastore.NewClient(ctx, project)
	if err != nil {
		return nil, err
	}

	var quotes []string
	query := datastore.NewQuery(entityName)
	it := client.Run(ctx, query)
	for {
		var p Post
		_, err = it.Next(&p)
		if err == iterator.Done {
			break
		}

		if err != nil {
			return nil, err
		}
		quotes = append(quotes, p.Quote)
	}

	return quotes, nil
}
