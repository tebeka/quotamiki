function on_ready() {
    load_quote();
    $('#refresh').click(load_quote);
}

function load_quote() {
    $.ajax({
	url: '/quote',
	dataType: 'text',
	error: on_error,
	success: on_quote,
    });
}

function on_quote(text) {
    var i = text.lastIndexOf('-');
    if (i > 0) {
	text = text.substring(0, i) + '<br/>' + text.substring(i, text.length);
    }
    $('#quote').html(text);
}

function on_error(xhr, status, err) {
    $('#quote').text('ERROR: ' + xhr.responseJSON.error);
}

$(on_ready);
