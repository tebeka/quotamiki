PROJECT = quotamiki

all:
	$(error please pick a target)

update-auth:
	GOOGLE_APPLICATION_CREDENTIALS=$(PWD)/gcloud.json \
	    go run update_auth.go auth.go $(CREATE) twitter.json

db-emulator:
	gcloud beta emulators datastore start --no-store-on-disk

test: check-emulator
	GOOGLE_APPLICATION_CREDENTIALS=$(PWD)/gcloud.json \
	DATASTORE_EMULATOR_HOST=localhost:8081 \
	    go test -v ./...

check-emulator:
	curl http://localhost:8081

httpd:
	GOOGLE_APPLICATION_CREDENTIALS=$(PWD)/gcloud.json \
	    go run .

deploy:
	@test -z "$(shell git status -s)" || \
	    (echo "error: local changes" && false)
	gcloud -q --project=$(PROJECT) app deploy
	gcloud -q --project=$(PROJECT) app deploy cron.yaml
	git tag -f deploy-$(shell date +%Y%m%dt%H%M%S)
	git push && git push --tags
