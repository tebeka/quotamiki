package main

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"net/url"

	"github.com/dghubble/oauth1"
)

// postTweet posts a tweet. Returns ID & error
func postTweet(ctx context.Context, status string, creds *TwitterCreds) (string, error) {
	config := oauth1.NewConfig(creds.AccessKey, creds.APISecretKey)
	token := oauth1.NewToken(creds.AccessToken, creds.AccessTokenSecret)

	client := config.Client(ctx, token)
	url := buildURL(status)

	resp, err := client.Post(url, "application/json", nil)
	if err != nil {
		return "", err
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		return "", fmt.Errorf(resp.Status)
	}

	var reply struct {
		ID string `json:"id_str"`
	}

	if err := json.NewDecoder(resp.Body).Decode(&reply); err != nil {
		return "", err
	}

	return reply.ID, nil
}

func buildURL(status string) string {
	u, _ := url.Parse("https://api.twitter.com/1.1/statuses/update.json")
	q := u.Query()
	q.Set("status", status)
	u.RawQuery = q.Encode()
	return u.String()
}
