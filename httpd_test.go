package main

import (
	"bytes"
	"io"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestQuote(t *testing.T) {
	require := require.New(t)

	w := httptest.NewRecorder()
	r := httptest.NewRequest("GET", "/quote", nil)
	quoteHandler(w, r)

	resp := w.Result()
	require.Equal(http.StatusOK, resp.StatusCode, "status")
	ctype := resp.Header.Get("Content-Type")
	require.Truef(strings.HasPrefix(ctype, "text/plain"), "content type: %s", ctype)

	var buf bytes.Buffer
	io.Copy(&buf, resp.Body)
	require.True(buf.String() != "", "empty response")
}

func TestCron(t *testing.T) {
	require := require.New(t)

	w := httptest.NewRecorder()
	r := httptest.NewRequest("GET", "/_/cron", nil)
	cronHandler(w, r)
	resp := w.Result()
	require.Equal(http.StatusUnauthorized, resp.StatusCode, "status")
}
