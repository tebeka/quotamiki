// +build ignore

// Update twitter authentication in Google's Secret Manager
package main

import (
	"context"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"os"

	secretmanager "cloud.google.com/go/secretmanager/apiv1beta1"
	secretmanagerpb "google.golang.org/genproto/googleapis/cloud/secretmanager/v1beta1"
)

func main() {
	log.SetFlags(0)
	var create bool
	flag.BoolVar(&create, "create", false, "create key")
	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, "usage: %s FILENAME\n", os.Args[0])
		flag.PrintDefaults()
	}
	flag.Parse()

	if flag.NArg() != 1 {
		log.Fatal("error: wrong number of arguments")
	}

	data, err := ioutil.ReadFile(flag.Arg(0))
	if err != nil {
		log.Fatalf("error: %s", err)
	}

	ctx := context.Background()
	client, err := secretmanager.NewClient(ctx)
	if err != nil {
		log.Fatalf("error: can't create client: %v", err)
	}

	if create {
		if err := createSecret(ctx, client); err != nil {
			log.Fatalf("error: can't create secret: %s", err)
		}
	}

	req := &secretmanagerpb.AddSecretVersionRequest{
		Parent: fmt.Sprintf("projects/%s/secrets/%s", projectID, secretID),
		Payload: &secretmanagerpb.SecretPayload{
			Data: data,
		},
	}

	ver, err := client.AddSecretVersion(ctx, req)
	if err != nil {
		log.Fatalf("error: can't set version: %v", err)
	}

	fmt.Printf("version: %s\n", ver.Name)
}

func createSecret(ctx context.Context, client *secretmanager.Client) error {
	req := &secretmanagerpb.CreateSecretRequest{
		Parent:   fmt.Sprintf("projects/%s", projectID),
		SecretId: secretID,
		Secret: &secretmanagerpb.Secret{
			Replication: &secretmanagerpb.Replication{
				Replication: &secretmanagerpb.Replication_Automatic_{
					Automatic: &secretmanagerpb.Replication_Automatic{},
				},
			},
		},
	}

	resp, err := client.CreateSecret(ctx, req)
	log.Printf("INFO: created %s", resp.Name)
	return err
}
