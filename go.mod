module gitlab.com/tebeka/quotamiki

go 1.14

require (
	cloud.google.com/go v0.54.0
	cloud.google.com/go/datastore v1.1.0
	github.com/dghubble/oauth1 v0.6.0
	github.com/stretchr/testify v1.5.1
	google.golang.org/api v0.20.0
	google.golang.org/genproto v0.0.0-20200312145019-da6875a35672
)
